#!/usr/bin/env node
const fs = require('fs');
const path = require('path');
const diffById = require('./src/diffById');
const cacheUtil = require('./src/cacheUtil');
const installer = require('./src/installer');

const powerlaceDir = process.env.POWERLACE_DIR
  ? path.resolve(process.cwd(), process.env.POWERLACE_DIR)
  : path.join(process.env.HOME, '.powerlace');

const configPath = path.join(powerlaceDir, 'config.js');
const cachePath = path.join(powerlaceDir, 'cache.json');

if (!fs.existsSync(powerlaceDir)) {
  try {
    fs.mkdirSync(powerlaceDir, { recursive: true })
  } catch (err) {
    console.error(`Could not create ${powerlaceDir}`, err);
    process.exit(1);
  }
}

if (!fs.existsSync(configPath)) {
  console.error(`Could not find config at ${configPath}`);
  console.error('See https://gitlab.com/yepreally/powerlace?nav_source=navbar#configuration');
  process.exit(1);
}

const config = require(configPath);
const cache = cacheUtil.read(cachePath);

const appsToUninstall = diffById(cache.apps, config.apps);
const appsToInstall = diffById(config.apps, cache.apps);

appsToInstall.forEach(app => {
  const installedApp = installer.install(app);
  if (installedApp) {
    cacheUtil.addAppToCache(cachePath, installedApp);
  }
});

appsToUninstall.forEach(app => {
  const uninstalledApp = installer.uninstall(app);
  if (uninstalledApp) {
    cacheUtil.removeAppFromCache(cachePath, uninstalledApp);
  }
});

if (appsToInstall.length === 0 && appsToUninstall.length === 0) {
  console.log('Your system is up to date!');
}
