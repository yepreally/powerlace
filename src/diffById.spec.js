const diffById = require('./diffById');

describe('diffById', () => {
  test.each`
    a                              | b                             | expectedResult
    ${undefined}                   | ${[]}                         | ${[]}
    ${[]}                          | ${undefined}                  | ${[]}
    ${undefined}                   | ${undefined}                  | ${[]}
    ${null}                        | ${[]}                         | ${[]}
    ${[]}                          | ${null}                       | ${[]}
    ${null}                        | ${null}                       | ${[]}
    ${'foo'}                       | ${[]}                         | ${[]}
    ${[]}                          | ${'foo'}                      | ${[]}
    ${'foo'}                       | ${'foo'}                      | ${[]}
    ${['foo', 'bar']}              | ${undefined}                  | ${['foo', 'bar']}
    ${['foo', 'bar']}              | ${null}                       | ${['foo', 'bar']}
    ${['foo', 'bar']}              | ${'foo'}                      | ${['foo', 'bar']}
    ${[{id: 'foo'}, {id: 'bar'}]}  | ${[{id: 'bar'}]}              | ${[{id: 'foo'}]}
    ${[{id: 'foo'}, {id: 'bar'}]}  | ${[{id: 'bar'}, {id: 'baz'}]} | ${[{id: 'foo'}]}
  `('diffById of $a and $b is $expectedResult', ({ a, b, expectedResult }) => {
    expect(diffById(a, b)).toEqual(expectedResult)
  });
});
