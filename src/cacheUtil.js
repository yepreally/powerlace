const fs = require('fs');
const path = require('path');
const fsUtil = require('./fsUtil');

const read = (cachePath) => fs.existsSync(cachePath) ? fsUtil.readJson(cachePath) : {};
const write = (cachePath, content) => fsUtil.writeJson(cachePath, content);

const addAppToCache = (cachePath, app) => {
  const cacheObj = read(cachePath);
  write(cachePath, { ...cacheObj, apps: [...(cacheObj.apps || []), app] });
};

const removeAppFromCache = (cachePath, app) => {
  const cacheObj = read(cachePath);
  write(cachePath, { ...cacheObj, apps: (cacheObj.apps || []).filter(cacheApp => cacheApp.id !== app.id) });
};

module.exports = {
  read,
  write,
  addAppToCache,
  removeAppFromCache,
};
