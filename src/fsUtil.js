const fs = require('fs');

const readJson = filePath => JSON.parse(fs.readFileSync(filePath, 'utf8'));
const writeJson = (filePath, contents) => fs.writeFileSync(filePath, JSON.stringify(contents, null, 2), 'utf8');

module.exports = {
  readJson,
  writeJson,
};
